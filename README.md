
##DB mysql
DB side preperations:

CREATE DATABASE manageusers;

USE manageusers;

CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`active` TINYINT(4) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
)

CREATE TABLE `log` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`loggedInAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)

dbconnection configurable in ./server/config/main

default:
	host: "localhost",
	user: "root",
	database: "manageusers",
	password: "admin"

## Table flags
'active' column:
	1 = user is active
	0 = user is deleted

## Available Scripts

In the project directory, you can run:
### `cd .\server\ ;  npm run start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Comments

* Authentication implementation needs improvement
* Not using eslint preset
* Routes security
* Not using sequelize package on server side
* Not using templates form email sending
* Using only ID for 'deleting', 'patching' and 'getting' user would be better
* Sending password to user is not good idea
* Using Sendgrid instead of Postmarkapp
* Haven't done automated testing
* Auth, Signup and Reset could be merged into one component
* Front lng can be changed from src/store/reducer.js

Time spent: ~ 42h

## Bugs

* Logged in user can be deleted from 'users' view
* Pagination not working in 'users' view


## API documentation
[https://manageusers.docs.apiary.io/](https://manageusers.docs.apiary.io/).



