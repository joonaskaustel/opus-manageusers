const config = require('../config/main');

const User = require('../models/user');

const nodemailer = require('nodemailer');

const sendgridTransport = require('nodemailer-sendgrid-transport');

const transporter = nodemailer.createTransport(sendgridTransport({
	auth: {
		api_key: config.sendGrid.api_key
	}
}))

exports.getUsers = (req, res) => {
	if (!req.session.isLoggedIn) {
		return res.status(401).send({
			status: 'error',
			message: 'access denied',
			validation: [],
			data: []
		});
	}

	User.fetchAll()
		.then(([rows, fielData]) => {
			const data = rows.map(item => {
				const id = item.id;
				const rowNum = rows.indexOf(item) + 1;
				const email = item.email;
				return { id, rowNum, email }
			})
			return res.status(200).send({
				status: 'success',
				message: 'users found',
				data: data
			});
		})
		.catch(err => {
			console.log(err);
		});
};

exports.getUser = (req, res) => {
    const userToGet = req.params.userId;

    User.getLog(userToGet)
        .then(([rows, fielData]) => {
            const log = [];
            rows.forEach(item => {
                log.push({  
                    email: item.email, 
                    date: item.loggedInAt
                });
            });

            return res.status(200).send({
                status: 'success',
                data: log
            });
        })
        .catch(err => {
            console.log(err);
        })
};

exports.deleteUser = (req, res) => {
    const userIdToDelete = req.params.userId;
    
    User.findById(userIdToDelete)
        .then(([rows, fielData]) => {
            const email = rows[0].email;
            
            User.deleteById(userIdToDelete)
                .then(result => {
                    if (result[0].affectedRows === 1) {
                        res.status(200).send({
                            status: 'success',
                            message: 'user deleted'
                        });
                        return transporter.sendMail({
                            from: 'manageusers@opus.com',
                            to: email,
                            subject: 'Account update',
                            html: `<p>Your account is deleted!</p>`
                        })
                    } else {
                        return res.status(404).send({
                            status: 'error',
                            message: result[0]
                        })
                    }
                })
                .catch(err => {
                    console.log(err);
                })
        })
        .catch(err => {
            console.log(err);
        })

};