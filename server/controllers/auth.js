const config = require('../config/main');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const randomGenerator = require('../utils/random');

const { validationResult } = require('express-validator/check');

const User = require('../models/user');

const transporter = nodemailer.createTransport(sendgridTransport({
	auth: {
		api_key: config.sendGrid.api_key
	}
}));

exports.getValidate = (req, res) => {
	res.send('validate email link');
};

exports.postLogin = (req, res) => {
	const data = req.body;
	const email = data.email;
	const password = data.password;
	const errors = validationResult(req);

	//Validate email, password, password confirm in auth route middleware
	if (!errors.isEmpty()) {
		return res.status(422).send({
			status: 'error',
			message: 'validation failed',
			auth: [
				{ userMatch: false }
			],
			validation: errors.array()
		})
	}

	User.findByEmail(email)
		.then(([rows, fielData]) => {

			//if user does not exist, return 
			if (!rows[0]) {
				return res.status(404).send({
					status: 'error',
					message: 'user not found',
					auth: [
						{ userMatch: false }
					],
					validation: []
				});
			}

			//if user exists, compare passwords
			bcrypt.compare(password, rows[0].password)
				.then((matching) => {
					if (matching) {
						//session
						req.session.isLoggedIn = true;
						
						//logging
						User.log(email);
						
						//jwt
						const payload = { user: email };
                		const options = { expiresIn: config.jwt.expiresIn, issuer: config.jwt.issuer };
                		const secret = config.jwt.secret;
                		const token = jwt.sign(payload, secret, options);
						
						return res.status(201).send({
							status: 'success',
							message: 'user found and password matches',
							auth: [
								{ userMatch: true },
								{ passwordMatch: true }
							],
							validation: [],
							token: token
						})
					}
					return res.status(401).send({
						status: 'error',
						message: 'wrong password',
						auth: [
							{ userMatch: true },
							{ passwordMatch: false }
						],
						validation: []
					})
				})
				.catch(err => {
					console.log(err);
				});
		})
		.catch(err => {
			console.log(err);
		});
};

exports.postSignup = (req, res) => {
	const data = req.body;
	const email = data.email;
	const password = data.password;
	const errors = validationResult(req);

	//Validate email, password, password confirm in auth route middleware
	if (!errors.isEmpty()) {
		return res.status(422).send({
			status: 'error',
			message: 'validation failed',
			validation: errors.array()
		})
	}

	//if validation !error, check if email exists
	User.findByEmail(email)
		.then(([rows, fielData]) => {

			//if user exists, return 
			if (rows[0]) {
				return res.status(409).send({
					status: 'error',
					message: 'user exists',
					validation: []
				});
			}

			//if user !exists, generate new, add to db and return success
			bcrypt.hash(password, 12)
				.then((hashedPassword) => {
					const user = new User(email, hashedPassword);
					user.register();
					return res.status(201).send({
						status: 'success',
						message: 'user does not exist and new user generated',
						validation: []
					})
				})
				.then(success => {
					return transporter.sendMail({
						from: 'manageusers@opus.com',
						to: email,
						subject: 'Welcome',
						html: '<p>Registering successful, validate account <a href="http://localhost:5000/validate"></a></p>'
					})
						.catch(err => {
							console.log(err);
						});
				});
		})
		.catch(err => {
			console.log(err);
		});

};

exports.postLogout = (req, res) => {
	req.session.destroy((err) => {
		return res.status(200).send({
			status: 'success',
			message: 'user logged out!'
		});
	});
};

exports.patchUser = (req, res) => {
	const data = req.body;
	const email = data.email;

	//validate
	const errors = validationResult(req);

	//Validate email, password, password confirm in auth route middleware
	if (!errors.isEmpty()) {
		return res.status(422).send({
			status: 'error',
			message: 'validation failed',
			validation: errors.array()
		})
	}

	User.findByEmail(email)
		.then(([rows, fielData]) => {

			//if user !exist, return 
			if (!rows[0]) {
				return res.status(404).send({
					status: 'error',
					message: 'user not found',
					validation: []
				});
			}

			const email = rows[0].email;
			const newPassword = randomGenerator();

			//hash new password and send unhashed password to user email
			bcrypt.hash(newPassword, 12)
				.then((hashedPassword) => {
					User.updateByEmail(email, hashedPassword)
						.then(result => {
							return res.status(200).send({
								status: 'success',
								message: 'user password updated!',
								validation: []
							});
						})
						.then(success => {
							return transporter.sendMail({
								from: 'manageusers@manageusers.com',
								to: email,
								subject: 'Password reset',
								html: `<p>Password reset successful, new password is:${newPassword}</p>`
							})
								.catch(err => {
									console.log(err);
								});
						})
						.catch(err => {
							console.log(err);
						})
				})
		})
		.catch(err => {
			console.log(err);
		});

};