const db = require('../utils/database');

module.exports = class User {

	constructor(email, password) {
		this.email = email;
		this.password = password;
	}

    register() {
        return db.execute('INSERT INTO users (email, password) VALUES (?, ?)', [this.email, this.password])
    }
	
	static log(email) {
        return db.execute('INSERT INTO log (email) VALUES (?)', [email])
    }

	static getLog(email) {
        return db.execute('SELECT * FROM log WHERE email = (?) ORDER BY loggedInAt DESC', [email])
    }

	static findByEmail(email) {
		return db.execute('SELECT email, password FROM users WHERE email = ? AND active = 1 ORDER BY createdAt DESC LIMIT 1', [email])
	}

    static findById(id) {
		return db.execute('SELECT email FROM users WHERE id = ? AND active = 1 ORDER BY createdAt DESC LIMIT 1', [id])
	}

	static updateByEmail(email, newPassword) {
		return db.execute('UPDATE users SET password = ? WHERE email = ? AND active = 1 ORDER BY createdAt DESC LIMIT 1', [newPassword, email])
	}

	static fetchAll() {
		return db.execute('SELECT id, email FROM users WHERE active = 1');
	}

	static deleteById(id) {
		return db.execute('UPDATE users SET active = 0 WHERE id = ? AND active = 1 ORDER BY createdAt DESC LIMIT 1', [id]);
	}

}