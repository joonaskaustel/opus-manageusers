const express = require('express');

const validateToken = require('../utils/validateJwt').validateToken;

const adminController = require('../controllers/admin');

const router = express.Router();

router.get('/users', validateToken, adminController.getUsers);

router.get('/users/:userId', validateToken, adminController.getUser);

router.delete('/users/:userId', validateToken, adminController.deleteUser);

module.exports = router;