const express = require("express");

const { body } = require("express-validator/check");

const authController = require("../controllers/auth");

const router = express.Router();

router.get("/validate", authController.getValidate);

router.post(
	"/login",
	[
		body("email", "Invalid email!")
			.isEmail()
			.trim()
	],
	authController.postLogin
);

router.post(
	"/signup",
	[
		body("email", "Invalid email!")
			.isEmail()
			.trim(),
		body("password", "Password too short!")
			.isLength({ min: 8 })
			.trim(),
		body("passwordConfirm")
			.trim()
			.custom((value, { req }) => {
				if (value !== req.body.password) {
					throw new Error("Passwords do not match");
				}
				return true;
			})
	],
	authController.postSignup
);

router.post("/logout", authController.postLogout);

router.patch(
	"/reset",
	[
		body("email", "Invalid email!")
			.isEmail()
			.trim()
	],
	authController.patchUser
);

module.exports = router;
