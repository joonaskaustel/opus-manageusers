const express = require('express');
const server = express();

const bodyparser = require('body-parser');

const session = require('express-session');

const randomGen = require('./utils/random');

const port = 5000;

const adminRoutes = require('./routes/admin');
const authRoutes = require('./routes/auth');

server.use(bodyparser.json());
server.use(session({
	secret: randomGen(),
	resave: false,
	saveUninitialized: false
}));

server.use('/admin', adminRoutes);
server.use(authRoutes);

server.listen(port, () => {
	console.log(`server started on ${port}`);
});