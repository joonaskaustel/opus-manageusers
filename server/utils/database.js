const config = require('../config/main');

const mysql = require("mysql2");

const pool = mysql.createPool({
	host: config.db.host,
	user: config.db.user,
	database: config.db.database,
	password: config.db.password
});

module.exports = pool.promise();
