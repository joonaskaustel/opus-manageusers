import React, { Component } from "react";
import { Route, NavLink, BrowserRouter, Redirect } from "react-router-dom";

import Auth from "./Components/Auth";
import Logout from "./Components/Logout";
import AddUser from "./Components/AddUser";
import Signup from "./Components/Signup";
import Reset from "./Components/Reset";
import Users from "./Components/Users";
import Details from "./Components/Details";

import { connect } from "react-redux";
import * as actionTypes from './store/actions'

import localized from './Localization/App';

class App extends Component {

    render() {
        
        localized.setLanguage(this.props.localLng);

        let redirect = null
        if (!this.props.isAuthenticated) {
            redirect = <Redirect to='/auth' />
        }
        
        return (
            <BrowserRouter>
                <div>
                    {redirect}
                    <nav className="navbar is-spaced has-shadow" role="navigation" aria-label="main navigation">
                        <div className='container'>
                            <div className="navbar-brand">

                                <a
                                    type='button'
                                    className={"navbar-burger burger " + (this.props.menuToggled ? 'is-active' : '')}
                                    aria-label="menu"
                                    aria-expanded="false"
                                    data-target="navbarBasicExample"
                                    onClick={this.props.onNavMenuToggle}
                                >
                                    <span aria-hidden="true" />
                                    <span aria-hidden="true" />
                                    <span aria-hidden="true" />
                                </a>

                            </div>

                            <div id="navbarBasicExample" className={"navbar-menu " + (this.props.menuToggled ? 'is-active' : '')}>
                                <div className="navbar-start">
                                    {
                                        this.props.isAuthenticated
                                            ? <NavLink className="navbar-item" to="/users">{localized.label.users}</NavLink>
                                            : null
                                    }
                                    {
                                        this.props.isAuthenticated
                                            ? <NavLink className="navbar-item" to="/add-user">{localized.label.addUser}</NavLink>
                                            : null
                                    }
                                    {
                                        this.props.isAuthenticated
                                            ? <NavLink className="navbar-item" to="/details">{localized.label.details}</NavLink>
                                            : null
                                    }
                                </div>

                                <div className="navbar-end">
                                    <div className="navbar-item">

                                        <div className="buttons">
                                            {
                                                this.props.isAuthenticated
                                                    ? <NavLink className="button is-danger" to="/logout" onClick={this.props.onLogout}>{localized.label.logout}</NavLink>
                                                    : <NavLink className="button is-primary is-outlined" to="/auth">{localized.label.login}</NavLink>
                                            }
                                            {
                                                this.props.isAuthenticated
                                                    ? null
                                                    : <NavLink className="button is-outlined" to="/signup">{localized.label.signup}</NavLink>
                                            }
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <div className="section">
                        <div className='container'>
                            <Route exact path="/" component={Auth} />
                            <Route path="/auth" component={Auth} />
                            <Route path="/logout" component={Logout} />
                            <Route path="/add-user" component={AddUser} />
                            <Route path="/signup" component={Signup} />
                            <Route path="/reset" component={Reset} />
                            <Route path="/users" component={Users} />
                            <Route path="/details" component={Details} />
                        </div>
                    </div>

                </div>
            </BrowserRouter>
        );
    }
}

const mapStateToProps = state => {
    return {
        menuToggled: state.menuToggled,
        isAuthenticated: state.isAuthenticated,
        localLng: state.localLng
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onNavMenuToggle: () => dispatch({ type: actionTypes.TOGGLE_NAV_MENU }),
        onLogout: () => dispatch({ type: actionTypes.LOGOUT })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
