import React, { Component } from "react";

import { Redirect } from 'react-router-dom';

import { connect } from "react-redux";

import axios from 'axios';

import localized from '../Localization/AddUser';

class Register extends Component {

	constructor() {
		super();
		this.state = {
			email: "",
			password: "",
			passwordConfirm: "",
			validation: [],
            userCreatedRedirect: false,
            userMatch: false
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		});
	}

	handleSubmit(e) {
		e.preventDefault();

		axios.post('/signup', {
			headers: { Authorization: this.props.token },
			email: this.state.email,
			password: this.state.password,
			passwordConfirm: this.state.passwordConfirm,
		})
			.then(res => {
				this.setState({
					...this.state,
					userCreatedRedirect: true
				});
			})
			.catch(error => {
                const rules = [];
                let userMatch = false;
				error.response.data.validation.map(item => {
					return rules.push(item);
				})

                if (error.response.status === 409) {
                    userMatch = true
                }

				this.setState({
					...this.state,
					message: error.response.data.message,
                    validation: rules,
                    userMatch: userMatch
				});
			});
	}

	render() {

		localized.setLanguage(this.props.localLng);

		let redirect = null;
		if (this.state.userCreatedRedirect) {
			redirect = <Redirect to='/users' />
		}

		return (
			<div>
				{redirect}
				<div className='columns'>
					<div className='column is-half is-offset-one-quarter'>

						<h2 className='title is-4'>{localized.title.addUser}</h2>

						{/* FORM */}
						<form onSubmit={this.handleSubmit} noValidate>

							{/* EMAIL */}
							<div className="field">
								<label className="label">{localized.label.email}</label>

								<div className="control has-icons-left has-icons-right">

									<input
										className={"input " + (this.state.validation.find(e => e.param === 'email') ? 'is-danger' : '')}
										type="email"
										name='email'
										placeholder={localized.label.email}
										value={this.state.email}
										onChange={this.handleChange}
									/>

									<span className="icon is-small is-left">
										<i className="fas fa-envelope" />
									</span>

								</div>

								<p className="help is-danger">{this.state.validation.find(e => e.param === 'email') ? localized.label.invalidEmail : ''}</p>
								<p className="help is-danger">{this.state.userMatch ? 'This email is taken!' : ''}</p>
							</div>
							{/* EMAIL */}

							{/* PASSWORD */}
							<div className="field">
								<label className="label">{localized.label.password}</label>

								<div className="control has-icons-left has-icons-right">
									<input
										className={"input " + (this.state.validation.find(e => e.param === 'password') ? 'is-danger' : '')}
										type="password"
										name='password'
										placeholder={localized.label.password}
										value={this.state.password}
										onChange={this.handleChange}
									/>

									<span className="icon is-small is-left">
										<i className="fas fa-key" />
									</span>
								</div>

								<p className="help is-danger">{this.state.validation.find(e => e.param === 'password') ? localized.label.weakPassword : ''}</p>
							</div>
							{/* PASSWORD */}

							{/* PASSWORDCONFIRM */}
							<div className="field">
								<label className="label">{localized.label.confirmPassword}</label>

								<div className="control has-icons-left has-icons-right">
									<input
										className={"input " + (this.state.validation.find(e => e.param === 'passwordConfirm') ? 'is-danger' : '')}
										type="password"
										name='passwordConfirm'
										placeholder={localized.label.confirmPassword}
										value={this.state.passwordConfirm}
										onChange={this.handleChange}
									/>

									<span className="icon is-small is-left">
										<i className="fas fa-key" />
									</span>
								</div>

								<p className="help is-danger">{this.state.validation.find(e => e.param === 'passwordConfirm') ? localized.label.invalidConfirmPassword : ''}</p>
							</div>
							{/* PASSWORDCONFIRM */}

							{/* SUBMIT */}
							<div className="field is-grouped">
								<div className="control">
									<button
										className="button is-link"
										type="submit"
									>
										{localized.label.addUser}
									</button>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
    return {
        menuToggled: state.menuToggled,
        isAuthenticated: state.isAuthenticated,
		localLng: state.localLng,
		token: state.token
    };
};

export default connect(mapStateToProps)(Register);
