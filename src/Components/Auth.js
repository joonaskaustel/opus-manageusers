import React, { Component } from "react";

import axios from 'axios';

import { NavLink } from "react-router-dom";
import { Redirect } from 'react-router-dom';

import { connect } from "react-redux";
import * as actionTypes from '../store/actions';

import localized from '../Localization/Auth';

class Auth extends Component {

	constructor() {
		super();
		this.state = {
			email: "",
			password: "",
			validation: [],
			auth: [
				{ userMatch: true },
				{ passwordMatch: true },
			],
			message: ''
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		})
	}

	handleSubmit(e) {
		e.preventDefault();

		axios.post('/login', {
			email: this.state.email,
			password: this.state.password
		})
			.then(res => {
				const token = res.data.token;
				const email = this.state.email;
				
				//call reducer
				this.props.onLoginSuccess(email, token);
			})
			.catch(error => {
				const rules = [];
				error.response.data.validation.map(item => {
					return rules.push(item);
				});

				const auth = error.response.data.auth.map(item => {
					return item;
				});

				this.setState({
					...this.state,
					auth: auth,
					message: error.response.data.message,
					validation: rules
				})
			})
	}

	render() {

		localized.setLanguage(this.props.localLng);

		let authRedirect = null;
		if (this.props.isAuthenticated) {
			authRedirect = <Redirect to='/users' />
		}

		return (
			<div>
				{authRedirect}
				<div className='columns'>
					<div className='column is-half is-offset-one-quarter'>

						<h2 className='title is-4'>{localized.title.login}</h2>
						<form onSubmit={this.handleSubmit} noValidate>

							{/* EMAIL */}
							<div className="field">
								<label className="label">{localized.label.email}</label>
								<div className="control has-icons-left has-icons-right">

									<input
										className={"input " + (this.state.validation.find(e => e.param === 'email') || !this.state.auth.find(e => e.userMatch) ? 'is-danger' : '')}
										type="email"
										name='email'
										placeholder={localized.label.email}
										value={this.state.email}
										onChange={this.handleChange}
									/>

									<span className="icon is-small is-left">
										<i className="fas fa-envelope" />
									</span>

								</div>
								<p className="help is-danger">{this.state.validation.find(e => e.param === 'email') || !this.state.auth.find(e => e.userMatch) ? localized.label.invalidEmail : ''}</p>
							</div>
							{/* EMAIL */}

							{/* PASSWORD */}
							<div className="field">
								<label className="label">{localized.label.password}</label>
								<div className="control has-icons-left has-icons-right">

									<input
										className={"input " + (!this.state.auth.find(e => e.passwordMatch) ? 'is-danger' : '')}
										type="password"
										name='password'
										placeholder={localized.label.password}
										value={this.state.password}
										onChange={this.handleChange}
									/>
									<span className="icon is-small is-left">
										<i className="fas fa-key" />
									</span>

								</div>

								<p className="help is-danger">{!this.state.auth.find(e => e.passwordMatch) ? localized.label.invalidPassword : ''}</p>
							</div>
							{/* PASSWORD */}

							{/* SUBMIT */}
							<div className="field is-grouped">
								<div className="control">
									<button
										className="button is-link"
										type="submit"
									>
										{localized.label.login}
									</button>
								</div>
								<div className="control">
									<NavLink className="navbar-item" to="/reset">{localized.label.reset}</NavLink>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.isAuthenticated,
		localLng: state.localLng
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onLoginSuccess: (email, token) => dispatch({ type: actionTypes.LOGIN, loggedIn: email, token: token })
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);