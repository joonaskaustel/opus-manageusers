import React, { Component } from 'react';

import axios from 'axios';

import { connect } from "react-redux";

import localized from '../Localization/Details';

class Details extends Component {

    constructor() {
        super();
        this.state = {
            logging: []
        }
    }

    componentDidMount() {
        const email = this.props.loggedInUser;
        axios.get("/admin/users/" + email, { headers: { Authorization: this.props.token } })
            .then(result => {
                this.setState({
                    ...this.state,
                    logging: result.data.data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }


    render() {

        localized.setLanguage(this.props.localLng);

        const log = this.state.logging.map(e => {
            return (
                <tr key={e.date}>
                    <td>{e.email}</td>
                    <td>{e.date}</td>
                </tr>
            )
        })

        return (
            <div>
                <div className='columns'>
                    <div className='column is-half is-offset-one-quarter'>
                        <h2 className='title is-4'>{localized.title.details}</h2>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>{localized.label.email}</th>
                                    <th>{localized.label.date}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {log}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        localLng: state.localLng,
        loggedInUser: state.loggedInUser,
        token: state.token
    };
};

export default connect(mapStateToProps)(Details);