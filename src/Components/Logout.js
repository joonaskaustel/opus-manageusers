import React, { Component } from "react";

import axios from 'axios';

import { Redirect } from 'react-router-dom';

class Logout extends Component {

	componentDidMount() {
		axios.post('/logout', {})
			.then(res => { 
				console.log(res);
			})
			.catch(err => {
				console.log(err);
			})
	}

	render() {
		return (
			<div>
				<Redirect to='/auth' />
			</div>
		);
	}
}

export default Logout;