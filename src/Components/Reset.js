import React, { Component } from 'react';

import { connect } from "react-redux";

import axios from 'axios';

import localized from '../Localization/Reset';

class Reset extends Component {

	constructor() {
		super();
		this.state = {
			status: "",
			email: "",
			resetConfirmModal: false,
			message: '',
			validation: []
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleReset = this.handleReset.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		});
	}

	handleReset(e) {
		e.preventDefault();
		this.setState({
			...this.state,
			resetConfirmModal: !this.state.resetConfirmModal
		});
	}

	handleSubmit() {
		axios.patch('/reset', {
			email: this.state.email
		})
			.then(res => {
				const status = res.data.status;

				const rules = [];
				res.data.validation.map(item => {
					return rules.push(item);
				});

				this.setState({
					...this.state,
					status: status,
					validation: rules,
					resetConfirmModal: !this.state.resetConfirmModal
				});
			})
			.catch(err => {
				const status = err.response.data.status;

				const rules = [];
				err.response.data.validation.map(item => {
					return rules.push(item);
				});

				this.setState({
					...this.state,
					status: status,
					validation: rules,
					resetConfirmModal: !this.state.resetConfirmModal
				});
			});
	}

	render() {

		localized.setLanguage(this.props.localLng);

		return (
			<div>
				<div className='columns'>
					<div className='column is-half is-offset-one-quarter'>

						<h2 className='title is-4'>{localized.title.reset}</h2>
						<form onSubmit={this.handleReset} noValidate>

							<div className="field">

								<label className="label">{localized.label.email}</label>

								<div className="control has-icons-left has-icons-right">

									<input
										className={"input " + (this.state.validation.find(e => e.param === 'email') || this.state.status === 'error' ? 'is-danger' : '')}
										type="email"
										name='email'
										placeholder={localized.label.email}
										value={this.state.email}
										onChange={this.handleChange}
									/>
									<span className="icon is-small is-left">
										<i className="fas fa-envelope" />
									</span>

								</div>

								<p className="help is-danger">{this.state.validation.find(e => e.param === 'email') || this.state.status === 'error' ? localized.label.invalidEmail : ''}</p>
								<p className="help is-success">{this.state.status === 'success' ? localized.label.passwordUpdated : ''}</p>

							</div>

							<div className="field is-grouped">
								<div className="control">
									<button
										className="button is-link"
										type="submit"
									>
										{localized.label.reset}
									</button>
								</div>
							</div>

						</form>

					</div>
				</div>

				<div className={"modal " + (this.state.resetConfirmModal ? 'is-active' : "")}>
					<div className="modal-background"></div>
					<div className="modal-card">

						<header className="modal-card-head">
							<p className="modal-card-title">{localized.modal.title}</p>
							<button className="delete" aria-label="close" onClick={this.handleReset}></button>
						</header>

						<section className="modal-card-body">
							{localized.modal.text}
            			</section>

						<footer className="modal-card-foot">
							<button className="button is-danger" onClick={this.handleSubmit}>{localized.modal.reset}</button>
							<button className="button " onClick={this.handleReset}>{localized.modal.cancel}</button>
						</footer>

					</div>
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => {
    return {
        menuToggled: state.menuToggled,
        isAuthenticated: state.isAuthenticated,
        localLng: state.localLng
    };
};

export default connect(mapStateToProps)(Reset);