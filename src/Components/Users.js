import React, { Component } from "react";

import axios from "axios";

import { connect } from "react-redux";
import * as actionTypes from '../store/actions';

import { Redirect } from "react-router-dom";

import localized from '../Localization/Users';

import "./Sass/Users.scss";

class Users extends Component {
    constructor() {
        super();
        this.state = {
            userList: [],
            message: "",
            deleteConfirm: false,
            deleteId: null
        };

        this.handleDelete = this.handleDelete.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getUsers = this.getUsers.bind(this);
    }

    handleDelete(e) {
        const idToDelete = e.target.parentElement.id;
        this.setState({
            ...this.state,
            deleteConfirm: !this.state.deleteConfirm,
            deleteId: idToDelete
        });
    }

    handleSubmit() {
        const idToDelete = this.state.deleteId;

        axios.delete("/admin/users/" + idToDelete, { headers: { Authorization: this.props.token } })
            .then(res => {
                console.log(res);
                this.getUsers();
            });

        this.setState({
            ...this.state,
            deleteConfirm: false,
            deleteId: null
        });
    }

    getUsers() {
        axios.get("/admin/users", { headers: { Authorization: this.props.token } })
            .then(res => {
                if (res.data.status === "error") {
                    this.setState({
                        ...this.state,
                        message: res.data.message
                    });
                }

                const users = res.data.data;
                this.setState({
                    ...this.state,
                    userList: users
                });
            });
    }

    componentDidMount() {
        this.getUsers();
    }

    render() {

        localized.setLanguage(this.props.localLng);

        const users = this.state.userList.map(user => {
            return (
                <li key={user.id}>
                    {user.rowNum}. {user.email}{" "}

                    <button
                        className="button is-danger is-pulled-right is-small is-outlined"
                        id={user.id}
                        onClick={this.handleDelete}
                        title="Delete person"
                    >
                        <span>{localized.label.delete}</span>
                        <span className="icon is-small">
                            <i className="fas fa-times"></i>
                        </span>
                    </button>

                </li>
            );
        });

        let redirect = null
        if (!this.props.isAuthenticated) {
            redirect = <Redirect to='/auth' />
        }

        return (
            <div className="Users">
                {redirect}
                <div className='columns'>
                    <div className='column is-half is-offset-one-quarter'>
                    
                        <h2 className="title is-4">{localized.title.allUsers}</h2>
                        <p>{this.state.message}</p>
                        <ul>{users}</ul>

                        <div className="section">
                            <nav
                                className="pagination is-centered"
                                role="navigation"
                                aria-label="pagination"
                            >
                                <button className="pagination-previous" target="_blank">{localized.label.previousPage}</button>
                                <button className="pagination-next" target="_blank">{localized.label.nextPage}</button>

                                <ul className="pagination-list">
                                    <li>
                                        <button className="pagination-link is-current" target="_blank" aria-label="Goto page 1">1</button>
                                    </li>
                                    <li>
                                        <button className="pagination-link" target="_blank" aria-label="Goto page 2">2</button>
                                    </li>
                                    <li>
                                        <button className="pagination-link" target="_blank" aria-label="Goto page 3">3</button>
                                    </li>
                                </ul>

                            </nav>
                        </div>

                    </div>
                </div>

                {/* MODAL */}
                <div
                    className={"modal " + (this.state.deleteConfirm ? "is-active" : "")}
                >
                    <div className="modal-background" />
                    <div className="modal-card">

                        <header className="modal-card-head">
                            <p className="modal-card-title">{localized.modal.title}</p>
                            <button
                                className="delete"
                                aria-label="close"
                                onClick={this.handleDelete}
                            />
                        </header>

                        <section className="modal-card-body">
                        {localized.modal.text}
            			</section>

                        <footer className="modal-card-foot">
                            <button className="button is-danger" onClick={this.handleSubmit}>
                                {localized.modal.delete}
        					</button>
                            <button className="button" onClick={this.handleDelete}>
                                {localized.modal.cancel}
							</button>
                        </footer>

                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        menuToggled: state.menuToggled,
        isAuthenticated: state.isAuthenticated,
        localLng: state.localLng,
        token: state.token
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onNavMenuToggle: () => dispatch({ type: actionTypes.TOGGLE_NAV_MENU }),
        onLogout: () => dispatch({ type: actionTypes.LOGOUT })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
