import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            email: "Email",
            password: "Password",
            confirmPassword: "Confirm password",
            addUser: "Add user",
            invalidEmail: 'This email is invalid!',
            weakPassword: 'This password is too weak!',
            invalidConfirmPassword: 'Passwords do not match!',
        },
        title: {
            addUser: "Add user"
        }
    },
    et: {
        label: {
            email: "E-post",
            password: "Parool",
            confirmPassword: "Korda parooli",
            addUser: "Lisa kasutaja",
            invalidEmail: 'Vale e-posti aadress!',
            weakPassword: 'Parool on liiga nõrk!',
            invalidConfirmPassword: 'Paroolid ei klapi!',
        },
        title: {
            addUser: "Kasutaja lisamine"
        }
    }
});

export default strings;