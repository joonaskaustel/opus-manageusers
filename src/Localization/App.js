import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            login: "Login",
            signup: "Sign up",
            users: "Users",
            addUser: "Add user",
            details: "Details",
            logout: "Log out",
        }
    },
    et: {
        label: {
            login: "Logi sisse",
            signup: "Registreeru",
            users: "Kasutajad",
            addUser: "Lisa kasutaja",
            details: "Detailid",
            logout: "Logi välja",
        }
    }
});

export default strings;