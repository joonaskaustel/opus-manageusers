import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            email: "Email",
            password: "Password",
            reset: "Reset password",
            login: "Login",
            invalidEmail: 'This email is invalid!',
            invalidPassword: 'Wrong password!',
        },
        title: {
            login: "Login"
        }
    },
    et: {
        label: {
            email: "E-post",
            password: "Parool",
            reset: "Taasta parool",
            login: "Logi sisse",
            invalidEmail: 'Vale e-posti aadress!',
            invalidPassword: 'Vale parool!',
        },
        title: {
            login: "Logi sisse"
        }
    }
});

export default strings;