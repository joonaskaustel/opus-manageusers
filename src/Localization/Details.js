import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            email: "Email",
            date: "Login date"
        },
        title: {
            details: "Details"
        }
    },
    et: {
        label: {
            email: "E-post",
            date: "Sisselogimise kuupäev"
        },
        title: {
            details: "Detailid"
        }
    }
});

export default strings;