import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            email: "Email",
            reset: "Reset password",
            passwordUpdated: 'Password updated!',
            invalidEmail: 'This email is invalid!'
        },
        title: {
            reset: "Reset password"
        },
        modal: {
            title: 'Reset password',
            text: 'Are you sure you want to reset your password?',
            reset: 'Reset',
            cancel: 'Cancel'
        }
    },
    et: {
        label: {
            email: "E-post",
            reset: "Taasta parool",
            passwordUpdated: 'Parool uuendatud!',
            invalidEmail: 'Vale e-posti aadress!'
        },
        title: {
            reset: "Taasta parool"
        },
        modal: {
            title: 'Taasta parool',
            text: 'Oled kindel, et soovid parooli taastada?',
            reset: 'Taasta',
            cancel: 'Tühista'
        }
    }
});

export default strings;