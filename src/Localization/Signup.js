import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            email: "Email",
            password: "Password",
            confirmPassword: "Confirm password",
            signup: "Sign up",
            invalidEmail: 'This email is invalid!',
            reservedEmail: 'This email is taken!',
            weakPassword: 'This password is too weak!',
            invalidConfirmPassword: 'Passwords do not match!',
        },
        title: {
            signup: "Sign up",
        }
    },
    et: {
        label: {
            email: "E-post",
            password: "Parool",
            confirmPassword: "Korda parooli",
            signup: "Registreeru",
            invalidEmail: 'Vale e-posti aadress!',
            reservedEmail: 'See e-posti aadress on juba võetud!',
            weakPassword: 'Parool on liiga nõrk!',
            invalidConfirmPassword: 'Paroolid ei klapi!',
        },
        title: {
            signup: "Registreeru",
        }
    }
});

export default strings;