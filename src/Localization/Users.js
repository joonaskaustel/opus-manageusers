import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    en: {
        label: {
            previousPage: "Previous page",
            nextPage: "Next page",
            delete: "Delete"
        },
        title: {
            allUsers: "All users by email"
        },
        modal: {
            delete: 'Delete',
            text: 'Are you sure you want to delete this person?',
            title: 'Confirm delete',
            cancel: 'Cancel'
        }
    },
    et: {
        label: {
            previousPage: "Eelmine",
            nextPage: "Järgmine",
            delete: "Kustuta"
        },
        title: {
            allUsers: "Kõik kasutajad"
        },
        modal: {
            delete: 'Kustuta',
            text: 'Oled kindel, et soovid selle kasutaja kustutada?',
            title: 'Kinnita kustutamine',
            cancel: 'Tühista'
        }
    }
});

export default strings;