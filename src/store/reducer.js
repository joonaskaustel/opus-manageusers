import * as actionTypes from './actions';

const initalState = {
    isAuthenticated: false,
    menuToggled: false,
    localLng: 'en',
    loggedInUser: '',
    token: ''
}

const reducer = (state = initalState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_NAV_MENU:
            return {
                ...state,
                menuToggled: !state.menuToggled
            }
        case actionTypes.LOGIN:
            return {
                ...state,
                isAuthenticated: true,
                loggedInUser: action.loggedIn,
                token: action.token
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                token: ''
            }
    }
    return state;
}

export default reducer;